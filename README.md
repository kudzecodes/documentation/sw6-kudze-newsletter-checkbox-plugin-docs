# Newsletter checkbox plugin for Shopware 6 Documentation

Updated for v1.1.3 version of the extension.

[[_TOC_]]

## Description

Its a simple plugin, that adds newsletter registration checkbox into Shopware's registration and checkout registration pages.

## Configuration

Plugin's configuration is self explanatory, plugin offers 4 configurable options:

1. If checked, newsletter box in register page will be checked by default
2. If checked, newsletter option will be disabled for guest users.
3. If checked, newsletter option will be hidden in checkout.
4. If checked, newsletter option will be hidden in account registration.

## FAQ

### Does this plugin support newsletter double opt in?

Yes, indeed this plugin does support newsletter double opt in. All of the double opt in configuration is tied to work with existing shopware configuration.
In order to enable newsletter double opt in, you need to activate `core.newsletter.doubleOptInRegistered` setting. 
Which can be done in administration panel: Settings > Shop > Newsletter > Double opt-in for registered customers.

### Does this plugin support customer double opt in?

Yes, indeed this plugin does support customer double opt in. All of the double opt in configuration is tied to work with existing shopware configuration.

In order to enable customer double opt in for regular customers, you need to activate `core.loginRegistration.doubleOptInRegistration` setting.
Which can be done in administration panel: `Settings > Shop > Log-in & sign-up > Double opt-in on sign-up`.

In order to enable customer double opt in for guest customers, you need to activate `core.loginRegistration.doubleOptInGuestOrder` setting. 
Which can be done in administration panel: `Settings > Shop > Log-in & sign-up > Double opt-in on guest orders`.

In case that customer double opt in is enabled, the newsletter recipient entry will only be added after customer verifies his email.

### Why does my customers needs to verify email twice. (Once for registration, once for newsletter).

This is because your shopware configuration tells the plugin that this should be done.
If you're using customer double opt in, then there is absolutely no reason to ask them to also verify email for newsletter registration.

In that case you should turn off `core.loginRegistration.doubleOptInRegistration` setting. But be aware that this setting is for both guest and non-guest customers.
See ["Does this plugin support customer double opt in?"](#does-this-plugin-support-newsletter-double-opt-in) section for additional information.

### How do i change text that is shown on the right of checkbox.

This can be achieved via Shopware 6 administration panel. You need to navigate to snippets configuration and change `register.subscribe_newsletter` snippet.
Snippets configuration can be found in administration panel: `Settings > Shop > Snippets`.

